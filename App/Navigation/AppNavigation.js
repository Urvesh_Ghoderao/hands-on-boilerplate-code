import { createStackNavigator } from "react-navigation";
import Auth from "../Containers/Auth";
import Login from "../Containers/Login";
import Register from "../Containers/Register";
import Home from "../Containers/Home";

import styles from "./Styles/NavigationStyles";

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    Auth: { screen: Auth },
    Login: {
      screen: Login,
      navigationOptions: { title: "Login" }
    },
    Register: {
      screen: Register,
      navigationOptions: { title: "Register" }
    },
    Home: {
      screen: Home,
      navigationOptions: { title: "Home" }
    }
  },
  {
    // Default config for all screens
    initialRouteName: "Auth",
    navigationOptions: {
      headerStyle: styles.header
    }
  }
);

export default PrimaryNav;
