import React from 'react';
import {BackHandler, Platform} from 'react-native';
import {createReduxBoundAddListener} from 'react-navigation-redux-helpers';
import {connect} from 'react-redux';
import AppNavigation from './AppNavigation';
import PropTypes from 'prop-types';

/**
 * ReduxNavigation Component
 */
class ReduxNavigation extends React.Component {
    /**
     * React Component Lifecycle method
     */
    componentWillMount() {
        if (Platform.OS === 'ios') return;
        BackHandler.addEventListener('hardwareBackPress', () => {
            const {dispatch, nav} = this.props;
            // change to whatever is your first screen, otherwise unpredictable results may occur
            // if (nav.routes.length === 1 && nav.routes[0].routeName === "Login") {
            //   return false;
            // }
            if (nav.index === 0) return false;
            // if (shouldCloseApp(nav)) return false
            dispatch({type: 'Navigation/BACK'});
            return true;
        });
    }

    /**
     * React Component Lifecycle method
     */
    componentWillUnmount() {
        if (Platform.OS === 'ios') return;
        BackHandler.removeEventListener('hardwareBackPress');
    }

    /**
     * @return {Component}
     */
    render() {
        return (
            <AppNavigation
                navigation={{
                    dispatch: this.props.dispatch,
                    state: this.props.nav,
                    addListener: createReduxBoundAddListener('root'),
                }}
            />
        );
    }
}

const mapStateToProps = ({nav, login}) => {
    return {
        nav: nav,
        userData: login.userData,
    };
};

ReduxNavigation.propTypes = {
    nav: PropTypes.object,
    dispatch: PropTypes.func,
};
export default connect(mapStateToProps)(ReduxNavigation);
