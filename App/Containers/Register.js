import React, {Component} from 'react';
import {View, Keyboard, Image, TouchableOpacity, ActivityIndicator, Alert} from 'react-native';
import {Button, Text, DatePicker, Label} from 'native-base';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import {getFormData} from '../Services/helper';
import PropTypes from 'prop-types';
import RegisterActions from '../Redux/RegisterRedux';
import moment from 'moment';
import slowlog from 'react-native-slowlog';

// Styles
import styles from './Styles/LaunchScreenStyles';
import {Fields} from '../Components';

/**
 * Register Component
 */
class Register extends Component {
    /**
     * @param {object} props
     * @param {object} state
     * @return {null}
     */
    static getDerivedStateFromProps(props, state) {
        const {registerData, error, navigation} = props;
        if (registerData) {
            navigation.replace('Login');
        }
        if (error) {
            Alert.alert('error', error);
        }
        return null;
    }

    /**
     * @param {object} props
     */
    constructor(props) {
        super(props);
        slowlog(this, /.*/);

        this.state = {
            username: '',
            usernameError: '',
            email: '',
            emailError: '',
            password: '',
            passwordError: '',
            confirmPassword: '',
            confirmPasswordError: '',
            firstname: '',
            firstnameError: '',
            lastname: '',
            lastnameError: '',
            gender: '',
            genderError: '',
            birthday: '',
            birthdayError: '',
            selfie: '',
            selfieError: '',
            avatar: '',
            avatarError: '',
        };

        this.onChangeText = this.onChangeText.bind(this);
        this.onSignUpPress = this.onSignUpPress.bind(this);
        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
        this.setDate = this.setDate.bind(this);
    }

    /**
     * @param {string} fieldName
     * @return {Function}
     */
    onChangeText(fieldName) {
        return (text) => {
            this.setState({
                [fieldName]: text,
                [fieldName + 'Error']: '',
            });
        };
    }

    /**
     * Date Picker function
     * @param {Object} newDate
     */
    setDate(newDate) {
        this.setState({birthday: moment(newDate).format('D/MM/YYYY'), birthdayError: ''});
    }

    /**
     * On Sign-in button
     */
    onSignUpPress() {
        const {username, email, password, confirmPassword, firstname, lastname, gender, selfie, avatar, birthday} = this.state;
        let usernameMessage = '';
        let emailMessage = '';
        let passwordMessage = '';
        let confirmPasswordMessage = '';
        let firstnameMessage = '';
        let lastnameMessage = '';
        let genderMessage = '';
        let selfieMessage = '';
        let birthdayMessage = '';
        let avatarMessage = '';
        if (
            username &&
            username.trim() &&
            email &&
            email.trim() &&
            password &&
            password.trim() &&
            confirmPassword &&
            confirmPassword.trim() &&
            firstname &&
            firstname.trim() &&
            lastname &&
            lastname.trim() &&
            gender &&
            gender.trim() &&
            selfie &&
            avatar &&
            birthday
        ) {
            const reg = new RegExp(/\S+@\S+\.\S+/);
            if (reg.test(email) && password === confirmPassword) {
                Keyboard.dismiss();
                const params = {
                    email,
                    password,
                    firstname,
                    lastname,
                    gender,
                    permission: 'relative',
                    birthday,
                    username,
                };
                const formData = getFormData(params);
                formData.append('avatar', {
                    uri: avatar,
                    type: 'image/jpg',
                    name: 'avatar',
                });
                formData.append('selfie', {
                    uri: selfie,
                    type: 'image/jpg',
                    name: 'selfie',
                });
                this.props.registerRequest(formData);
            } else {
                emailMessage = !reg.test(email) ? 'Invalid email-id' : '';
                confirmPasswordMessage = password !== confirmPassword ? 'Password mismatch error' : '';
                this.setState({
                    emailError: emailMessage || '',
                    confirmPasswordError: confirmPasswordMessage || '',
                });
            }
        } else {
            usernameMessage = 'Username field is required';
            emailMessage = 'Email field is required';
            passwordMessage = 'Password field is required';
            confirmPasswordMessage = 'Retype field is requried';
            firstnameMessage = 'First Name field is required';
            lastnameMessage = 'Last Name field is required';
            genderMessage = 'Gender field is required';
            selfieMessage = 'Selfie field is required';
            avatarMessage = 'Avatar field is required';
            birthdayMessage = ' Birth field is required';
            this.setState({
                usernameError: !username || !username.trim() ? usernameMessage : '',
                emailError: !email || !email.trim() ? emailMessage : '',
                passwordError: !password || !password.trim() ? passwordMessage : '',
                confirmPasswordError: !confirmPassword || !confirmPassword.trim() ? confirmPasswordMessage : '',
                firstnameError: !firstname || !firstname.trim() ? firstnameMessage : '',
                lastnameError: !lastname || !lastname.trim() ? lastnameMessage : '',
                genderError: !gender || !gender.trim() ? genderMessage : '',
                selfieError: !selfie ? selfieMessage : '',
                avatarError: !avatar ? avatarMessage : '',
                birthdayError: !birthday ? birthdayMessage : '',
            });
        }
    }

    /**
     * Image picker function
     * @param {String} keyName
     * @return {Function}
     */
    selectPhotoTapped(keyName) {
        return () => {
            const options = {
                quality: 1.0,
                maxWidth: 500,
                maxHeight: 500,
                storageOptions: {
                    skipBackup: true,
                },
            };
            ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    console.log('User cancelled photo picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);
                } else {
                    const source = {uri: response.uri};

                    // You can also display the image using data:
                    // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                    this.setState({
                        [keyName]: source,
                        [keyName + 'Error']: '',
                    });
                }
            });
        };
    }

    /**
     * @return {Component}
     */
    render() {
        const {
            username,
            usernameError,
            emailError,
            email,
            password,
            passwordError,
            confirmPassword,
            confirmPasswordError,
            firstname,
            firstnameError,
            lastname,
            lastnameError,
            gender,
            genderError,
            selfie,
            selfieError,
            avatar,
            avatarError,
            birthday,
            birthdayError,
        } = this.state;
        const {navigation, loading} = this.props;
        const selfieMsg = selfieError ? 'Selfie is required' : 'Selfie';
        const avatarMsg = avatarError ? 'Avatar is required' : 'Avatar';
        return (
            <KeyboardAwareScrollView>
                <View style={styles.mainContainer}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-evenly',
                            marginTop: 10,
                        }}
                    >
                        <View style={{alignItems: 'center'}}>
                            <TouchableOpacity
                                onPress={this.selectPhotoTapped('selfie')}
                                style={{
                                    height: 100,
                                    width: 100,
                                    borderRadius: 50,
                                    backgroundColor: 'rgba(0,0,0,0.3)',
                                }}
                            >
                                <Image source={selfie} style={{height: 100, width: 100, borderRadius: 50}} resizeMode="cover" />
                            </TouchableOpacity>
                            <Text style={{paddingTop: 5, color: selfieError ? 'red' : 'black'}}>{selfieMsg}</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <TouchableOpacity
                                onPress={this.selectPhotoTapped('avatar')}
                                style={{
                                    height: 100,
                                    width: 100,
                                    borderRadius: 50,
                                    backgroundColor: 'rgba(0,0,0,0.3)',
                                }}
                            >
                                <Image source={avatar} style={{height: 100, width: 100, borderRadius: 50}} resizeMode="cover" />
                            </TouchableOpacity>
                            <Text style={{paddingTop: 5, color: avatarError ? 'red' : 'black'}}>{avatarMsg}</Text>
                        </View>
                    </View>
                    <Fields
                        label="Username *"
                        value={username}
                        errorMessage={usernameError}
                        placeholder="user123"
                        autoCapitalize="none"
                        onChangeText={this.onChangeText('username')}
                    />
                    <Fields
                        label="Email *"
                        value={email}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        errorMessage={emailError}
                        placeholder="user@domain.com"
                        onChangeText={this.onChangeText('email')}
                    />
                    <Fields
                        label="Password *"
                        value={password}
                        autoCapitalize="none"
                        keyboardType="email-address"
                        errorMessage={passwordError}
                        onChangeText={this.onChangeText('password')}
                        secureTextEntry={true}
                    />
                    <Fields
                        label="Retype Password *"
                        value={confirmPassword}
                        autoCapitalize="none"
                        errorMessage={confirmPasswordError}
                        onChangeText={this.onChangeText('confirmPassword')}
                        secureTextEntry={true}
                    />
                    <Fields label="First Name *" value={firstname} errorMessage={firstnameError} onChangeText={this.onChangeText('firstname')} />
                    <Fields label="Last Name *" value={lastname} errorMessage={lastnameError} onChangeText={this.onChangeText('lastname')} />
                    <Fields
                        label="Gender *"
                        value={gender}
                        placeholder="male or female"
                        errorMessage={genderError}
                        onChangeText={this.onChangeText('gender')}
                        autoCapitalize="none"
                    />
                    <View
                        style={{
                            marginTop: 10,
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                        }}
                    >
                        <Label style={{color: birthdayError ? 'red' : 'grey', fontSize: 15}}>Birthday *</Label>
                        <DatePicker
                            style={{marginTop: 20}}
                            value={birthday}
                            defaultDate={new Date()}
                            minimumDate={new Date(1980, 1, 1)}
                            maximumDate={new Date()}
                            locale={'en'}
                            timeZoneOffsetInMinutes={undefined}
                            modalTransparent={false}
                            animationType={'fade'}
                            androidMode={'default'}
                            placeHolderText="select birthday"
                            textStyle={{color: 'black'}}
                            placeHolderTextStyle={{color: 'rgba(0,0,0,0.6)'}}
                            onDateChange={this.setDate}
                        />
                    </View>
                    <View style={{marginTop: 10, marginHorizontal: 20}}>
                        <Button full rounded onPress={this.onSignUpPress} disabled={loading}>
                            {loading ? <ActivityIndicator /> : <Text>Sign Up</Text>}
                        </Button>
                    </View>
                    <Text style={{marginVertical: 10, textAlign: 'center', fontSize: 15}}>OR</Text>
                    <Text onPress={() => navigation.replace('Login')} style={{color: 'blue', textAlign: 'center', fontSize: 12}}>
                        Sign In
                    </Text>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

Register.propTypes = {
    navigation: PropTypes.object,
    loading: PropTypes.bool,
    registerData: PropTypes.object,
    error: PropTypes.any,
    registerRequest: PropTypes.func,
};

const mapStateToProps = ({register}) => {
    return {
        loading: register.loading,
        registerData: register.registerData,
        error: register.error,
    };
};

const mapDispatchToProps = {
    registerRequest: RegisterActions.registerRequest,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);
