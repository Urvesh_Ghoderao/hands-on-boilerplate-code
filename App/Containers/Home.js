import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
import {StackActions, NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import LoginActions from '../Redux/LoginRedux';
import PropTypes from 'prop-types';
import slowlog from 'react-native-slowlog';

// Styles
/**
 * Home Component
 */
class Home extends Component {
    /**
     *@param {object} props
     */
    constructor(props) {
        super(props);
        slowlog(this, /.*/);
        this.onLogoutPress = this.onLogoutPress.bind(this);
    }

    /**
     * On logout press clear userdata
     */
    onLogoutPress() {
        this.props.logoutSuccess();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Login'})],
        });
        this.props.navigation.dispatch(resetAction);
    }

    /**
     * @return {Component}
     */
    render() {
        const {firstname} = this.props.userData;
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text onPress={() => this.props.navigation.replace('Register')}>Welcome to {firstname}</Text>
                <TouchableOpacity onPress={this.onLogoutPress} style={{padding: 10, backgroundColor: 'grey'}}>
                    <Text>Logout</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStatesToProps = ({login}) => {
    return {
        userData: login.userData,
    };
};

const mapDispatchToProps = {
    logoutSuccess: LoginActions.logoutSuccess,
};

Home.propTypes = {
    navigation: PropTypes.object,
    logoutSuccess: PropTypes.func,
    userData: PropTypes.object,
};

export default connect(
    mapStatesToProps,
    mapDispatchToProps
)(Home);
