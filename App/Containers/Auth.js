import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import PropTypes from 'react-native';
// Styles

/**
 * Auth Component
 */
class Auth extends Component {
    /**
     * Lifecycle method React Component
     */
    componentDidMount() {
        const {userData} = this.props;
        const routeName = userData ? 'Home' : 'Login';
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName})],
        });
        this.props.navigation.dispatch(resetAction);
    }

    /**
     * @return {Component}
     */
    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator />
            </View>
        );
    }
}

const mapStatesToProps = ({login}) => {
    return {
        userData: login.userData,
    };
};

export default connect(
    mapStatesToProps,
    null
)(Auth);

Auth.propTypes = {
    userData: PropTypes.object,
    navigation: PropTypes.object,
};
