import React, {Component} from 'react';
import {View, Keyboard, ActivityIndicator, Alert} from 'react-native';
import {Button, Text} from 'native-base';
import {StackActions, NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import LoginActions from '../Redux/LoginRedux';
// Styles
import styles from './Styles/LaunchScreenStyles';
import {Fields} from '../Components';
import PropTypes from 'prop-types';

/**
 * Login Component
 */
class Login extends Component {
    /**
     * @param {object} props
     * @param {object} state
     * @return {null}
     */
    static getDerivedStateFromProps(props, state) {
        const {userData, error, navigation} = props;

        if (userData) {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Home'})],
            });
            navigation.dispatch(resetAction);
        }
        if (error) {
            Alert.alert('error', error);
        }
        return null;
    }

    /**
     * @param {object} props
     */
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            emailError: '',
            password: '',
            passwordError: '',
        };
        this.onChangeText = this.onChangeText.bind(this);
        this.onSignInPress = this.onSignInPress.bind(this);
    }

    /**
     * Life cycle method
     */
    componentDidMount() {
        // this.props.clearState();
        console.log('Create state is ', this.props.clearState);
    }

    /**
     * Lifecycle method of Component
     */
    componentWillUnmount() {
        // this.props.clearState();
    }

    /**
     * @param {string} fieldName
     * @return {Function}
     */
    onChangeText(fieldName) {
        return (text) => {
            this.setState({
                [fieldName]: text,
                [fieldName + 'Error']: '',
            });
        };
    }

    onSignInPress() {
        this.inputRef.focus();
    }

    /**
     * On Sign-in button
     */
    onSignInPress1() {
        const {email, password} = this.state;
        let emailMessage = '';
        let passwordMessage = '';
        if (email && email.trim() && password && password.trim()) {
            const reg = new RegExp(/\S+@\S+\.\S+/);
            if (reg.test(email)) {
                Keyboard.dismiss();
                this.props.loginRequest(email, password);
            } else {
                emailMessage = 'Invalid email-id';
                this.setState({
                    emailError: emailMessage || '',
                });
            }
        } else {
            emailMessage = 'Email field is required';
            passwordMessage = 'Password field is required';
            this.setState({
                emailError: !email || !email.trim() ? emailMessage : '',
                passwordError: !password || !password.trim() ? passwordMessage : '',
            });
        }
    }

    /**
     * @return {Component}
     */
    render() {
        const {emailError, email, password, passwordError} = this.state;
        const {navigation, loading} = this.props;
        return (
            <View style={styles.mainContainer}>
                <Fields
                    label="Email"
                    value={email}
                    errorMessage={emailError}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    placeholder="user@domain.com"
                    onChangeText={this.onChangeText('email')}
                />
                <Fields
                    label="Password"
                    value={password}
                    ref={(input) => (this.inputRef = input)}
                    errorMessage={passwordError}
                    autoCapitalize="none"
                    onChangeText={this.onChangeText('password')}
                    secureTextEntry={true}
                />
                <View style={{marginTop: 10, marginHorizontal: 20}}>
                    <Button full rounded onPress={this.onSignInPress} disabled={loading}>
                        {loading ? <ActivityIndicator /> : <Text>Sign In</Text>}
                    </Button>
                </View>
                <Text style={{marginVertical: 10, textAlign: 'center', fontSize: 15}}>OR</Text>
                <Text onPress={() => navigation.replace('Register')} style={{color: 'blue', textAlign: 'center', fontSize: 12}}>
                    Create Account
                </Text>
            </View>
        );
    }
}

const mapStatesToProps = ({login}) => {
    return {
        loading: login.loading,
        userData: login.userData,
        error: login.error,
    };
};

const mapDispatchToProps = {
    loginRequest: LoginActions.loginRequest,
    clearState: LoginActions.clearLoading,
};

export default connect(
    mapStatesToProps,
    mapDispatchToProps
)(Login);

Login.propTypes = {
    navigation: PropTypes.object,
    loginRequest: PropTypes.func,
    clearState: PropTypes.func,
    loading: PropTypes.bool,
    userData: PropTypes.object,
    error: PropTypes.string,
};
