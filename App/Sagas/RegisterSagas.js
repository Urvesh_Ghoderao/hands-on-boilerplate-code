import {put, call} from 'redux-saga/effects';
import RegisterAction from '../Redux/RegisterRedux';

/**
 * @param {Object} api
 * @param {Object} action
 */
export function* doRegister(api, action) {
    const {form} = action;
    const response = yield call(api.registerUser, form);
    const {status, message, data} = response.data;
    if (status) {
        yield put(RegisterAction.registerSuccess(data));
    } else {
        yield put(RegisterAction.registerFailure(message));
    }
}
