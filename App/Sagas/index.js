import {takeLatest, all} from 'redux-saga/effects';
import API from '../Services/Api';
import FixtureAPI from '../Services/FixtureApi';
import DebugConfig from '../Config/DebugConfig';

/* ------------- Types ------------- */

import {StartupTypes} from '../Redux/StartupRedux';
import {GithubTypes} from '../Redux/GithubRedux';
import {LoginTypes} from '../Redux/LoginRedux';
import {RegisterTypes} from '../Redux/RegisterRedux';

/* ------------- Sagas ------------- */

import {startup} from './StartupSagas';
import {getUserAvatar} from './GithubSagas';
import {doLogin, doLogout, clearLoadingState, removeError} from './LoginSagas';
import {doRegister} from './RegisterSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

/* ------------- Connect Types To Sagas ------------- */

/**
 * Root Saga
 */
export default function* root() {
    yield all([
        // some sagas only receive an action
        takeLatest(StartupTypes.STARTUP, startup),

        // some sagas receive extra parameters in addition to an action
        takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),
        takeLatest(LoginTypes.LOGIN_REQUEST, doLogin, api),
        takeLatest(LoginTypes.LOGOUT_SUCCESS, doLogout),
        takeLatest(LoginTypes.LOGIN_FAILURE, removeError),
        // takeLatest(LoginTypes.CLEAR_LOADING, clearLoadingState),
        takeLatest(RegisterTypes.REGISTER_REQUEST, doRegister, api),
    ]);
}
