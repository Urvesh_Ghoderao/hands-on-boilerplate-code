import {call, put} from 'redux-saga/effects';
import LoginActions from '../Redux/LoginRedux';
import App from '../Containers/App';

/**
 * @param {Object} api
 * @param {Object} action
 */
export function* doLogin(api, action) {
    const {email, password} = action;
    const response = yield call(api.loginUser, {email, password});
    if (response.status) {
        const {status, message, data} = response.data;
        if (status) {
            yield put(LoginActions.loginSuccess(data));
        } else {
            yield put(LoginActions.loginFailure(message));
        }
    } else {
        yield put(LoginActions.loginFailure('Something Went Wrong try again later'));
    }
}

/**
 * @return {object}
 */
export function* doLogout() {
    return null;
}

/**
 * Remove Error
 */
export function* removeError() {
    yield put(LoginActions.removeError());
}
