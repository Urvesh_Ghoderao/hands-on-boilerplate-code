import {createReducer, createActions} from 'reduxsauce';
import immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
    registerRequest: ['form'],
    registerSuccess: ['data'],
    registerFailure: ['message'],
});

export const RegisterTypes = Types;
export default Creators;

export const INITIAL_STATE = immutable({
    loading: false,
    registerData: null,
    error: null,
});

/* ------------- Reducers ------------- */

export const request = () => {
    return {...INITIAL_STATE, loading: true};
};

export const success = (state, action) => {
    const {data} = action;
    return {...INITIAL_STATE, registerData: data};
};

export const failure = (state, action) => {
    const {message} = action;
    return {...INITIAL_STATE, error: message};
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.REGISTER_REQUEST]: request,
    [Types.REGISTER_SUCCESS]: success,
    [Types.REGISTER_FAILURE]: failure,
});
