import {createReducer, createActions} from 'reduxsauce';
import immutable from 'seamless-immutable';
import {REHYDRATE} from 'redux-persist';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
    loginRequest: ['email', 'password'],
    loginSuccess: ['data'],
    loginFailure: ['message'],
    removeError: null,
    logoutSuccess: null,
    clearLoading: null,
    rehydrate: null,
});

export const LoginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = immutable({
    loading: false,
    userData: null,
    error: null,
});

/* ------------- Reducers ------------- */

export const request = (state, action) => {
    return {...INITIAL_STATE, loading: true};
};

export const success = (state, action) => {
    const {data} = action;
    return {...INITIAL_STATE, userData: data};
};

export const failure = (state, action) => {
    const {message} = action;
    return {...INITIAL_STATE, error: message};
};

export const removeError = (state, action) => {
    return {...INITIAL_STATE};
};

export const logout = () => {
    return {...INITIAL_STATE};
};

export const customize = (state, action) => {
    console.log('From customize', action);
    const {payload} = action;

    return {...state, userData: payload.login ? payload.login.userData : payload.userData};
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.LOGIN_REQUEST]: request,
    [Types.LOGIN_SUCCESS]: success,
    [Types.LOGIN_FAILURE]: failure,
    [Types.LOGOUT_SUCCESS]: logout,
    [Types.REMOVE_ERROR]: removeError,
    [REHYDRATE]: customize,
});
