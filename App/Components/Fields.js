import React, {Component} from 'react';
import {View} from 'react-native';
import {Images} from '../Themes';
import PropTypes from 'prop-types';

import {Text, Item, Input, Label} from 'native-base';

/**
 * Fields Component
 */
class Fields extends Component {
    focus() {
        this.inputRef._root.focus();
    }

    /**
     * @return {Component}
     */
    render() {
        const {label, errorMessage, ...props} = this.props;
        return (
            <View style={{marginTop: 5}}>
                <Item
                    stackedLabel
                    style={{
                        borderBottomColor: errorMessage ? 'red' : 'grey',
                        borderBottomWidth: 1,
                    }}
                >
                    <Label style={{color: 'grey', fontSize: 15}}>{label}</Label>
                    <Input autoCorrect={false} ref={(input) => (this.inputRef = input)} {...props} />
                </Item>
                {errorMessage ? (
                    <Text
                        style={{
                            textAlign: 'center',
                            color: 'red',
                            fontSize: 12,
                            marginTop: 5,
                        }}
                    >
                        {errorMessage}
                    </Text>
                ) : null}
            </View>
        );
    }
}

Fields.propTypes = {
    errorMessage: PropTypes.string,
    label: PropTypes.string,
};

export default Fields;
